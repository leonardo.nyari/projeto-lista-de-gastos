import React, { useEffect, useState } from 'react';
import instanciaAxios from './ajax/instanciaAxios';
import moment from 'moment';
import 'moment/locale/pt-br';
import './listaTarefas.css'
import CurrencyInput from 'react-currency-input-field';




const ListaTarefas = () =>  {

    const [listaCategorias, setListaCategorias] = useState([]);
    const [listaTarefas, setListaTarefas] = useState([]);
    const [listaValores, setListaValores] = useState([]);    
    const [listaData, setListaData] = useState([]);
    const [listaTurno, setListaTurno] = useState([]);
    const [descricaoNovoItem, setDescricaoNovoItem] = useState('');
    const [categoriaNovoItem, setCategoriaNovoItem] = useState('');
    const [dataNovoItem, setDataNovoItem] = useState('');
    const [turnoNovoItem, setTurnoNovoItem] = useState('');
    const [valorNovoItem, setValorNovoItem] = useState('');
    const [alertaNovoItem, setAlertaNovoItem] = useState('desligado');
    
    


    useEffect(() => {
        pegarCategorias();
        pegarTarefas();
        pegarTurno();
        pegarData();
    },[]); 




    const pegarCategorias = async () => {

    try {

      const resposta = await instanciaAxios.get('../json/categorias.json');
    //   console.log(`Resultado: ${ JSON.stringify(resposta.data)}`);
      

      setListaCategorias(resposta.data.categorias);

    } catch (error) {
        console.log(error.message);
    }

    };




    const pegarTarefas = async () => {

        try {
    
          const resposta = await instanciaAxios.get('../json/tarefas.json');
                  
          setListaTarefas(resposta.data.tarefas);
    
        } catch (error) {
            console.log(error.message);
        }
    
        };



            const pegarData = async () => {

                try {
            
                  const resposta = await instanciaAxios.get('../json/tarefas.json');
                          
                  setListaData(resposta.data.tarefas);
                  setListaValores(resposta.data.tarefas);
                  
            
                } catch (error) {
                    console.log(error.message);
                }
            
                };




            const pegarTurno = async () => {

                try {
            
                  const resposta = await instanciaAxios.get('../json/turnos.json');
                          
                  setListaTurno(resposta.data.turnos);
            
                } catch (error) {
                    console.log(error.message);
                }
            
                };


    const OpcoesCategoriasComponente = () => {

        const listaCategoriasJSX = listaCategorias.map( ( item ) => {
            // console.log(`Conteúdo da variável item: ${item}`);
            
            return (
                <option
                value={item.id} 
                key={item.id}>
                    { item.descricao } 
                </option>
                
            );
        });

        return listaCategoriasJSX;
    };


    const OpcoesTurnoComponente = () => {

        const listaTurnoJSX = listaTurno.map( ( item ) => {
            // console.log(`Conteúdo da variável item: ${item}`);
            
            return (
                <div key={item.id}>
                    <label>
                    <input 
                    type="radio" 
                    name="turno" 
                    value={item.id}
                    checked={ item.id === turnoNovoItem }
                    onChange={(evento) => setTurnoNovoItem( evento.target.value) } />{item.rotulo}</label>
                </div>
                
            );
        });

        return listaTurnoJSX;
    
    };


    const AlertaIconeComponente = () => {
        return (
            <img src="images/despertador.png" alt="" style={{width: '12px', marginLeft: '0px'}}/>
        );
    };


    const CorpoTabelaComponente = () => {
        
        return(

            <tbody>
                { listaTarefas.map((item) => {
                    return (
                       <LinhaTabelaComponente 
                        key={item.id}
                        id={item.id}
                        descricao={item.descricao}
                        categoria={item.idCategoria}
                        turno={item.idTurno}
                        valores={item.valores} 
                        data= {item.data}
                        alerta= {item.alerta}/>
                    );
                })}
               
            </tbody>

        );
    };



    const LinhaTabelaComponente = (props) => {

            const _categoria = listaCategorias.find((item) => {
                return item.id === props.categoria;
            });
    
            let _turno = null;
            _turno = listaTurno ? listaTurno.find(item => {
                return item.id === props.turno;
            }) : null;

            let _data = null;
            _data = listaData ? listaData.find(item => {
                return item.id === props.data;
            }) : null;

            const _alerta = props.alerta === 'ligado' ? <AlertaIconeComponente/> : null;
        
        return (
            <tr>
                <td>{props.descricao} {_alerta} </td>
                <td>{_categoria.descricao}</td>
                <td>{props.data}</td>
                <td>{_turno ? _turno.rotulo : null}</td>
                <td>{props.valores.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}</td>
                <td>
                    <img id="lixeira" src="images/lixeira.png" alt="" style={{width: '15px', marginLeft: '6px'}}
                    onClick={() => {removerItem(props.id)} } />
                    
                </td>
            </tr>
        );
    };

    let total = 0    
    listaTarefas.forEach(item => {      
        total += parseFloat(item.valores) 
        
    })

    



    const incluirItem = (event) => {
        // console.log('usuário clicou');
        
        // console.log(`O usuário escolheu a categoria: ${categoriaNovoItem}`)
        // console.log(`O usuário preencheu a descrição: ${descricaoNovoItem}`)

        // event.preventDefault();

        if( categoriaNovoItem && descricaoNovoItem && valorNovoItem && dataNovoItem) {
                        
            
            let novaData = moment(dataNovoItem)

            let idUltimoElemento = "0"
                       
            if(listaTarefas.length){
                const indiceUltimoElemento = listaTarefas.length - 1;
                const ultimoElemento = listaTarefas[indiceUltimoElemento];
                idUltimoElemento = ultimoElemento.id;
            }

           
            const idNovoItem = parseInt (idUltimoElemento) + 1;
            // console.log(idNovoItem);


            const novoItem = {
                'id': idNovoItem,    
                'descricao': descricaoNovoItem,
                'idCategoria': categoriaNovoItem,
                'valores': parseFloat(valorNovoItem.replace(",",".")),
                'idTurno': turnoNovoItem,
                'data': novaData.format('DD/MM/YYYY, dddd'),
                'alerta': alertaNovoItem


            };

            setListaTarefas( [...listaTarefas, novoItem] );
        } else {
            alert('Por favor, preencha os campos categoria e descrição.')
        }

    };
    
    const removerItem = (idSelecionado) => {
        // console.log(`o id foi selecionado ${idSelecionado}`);

        const _listaTarefas = listaTarefas.filter((item) => {
           return item.id !== idSelecionado;
        });
        
        setListaTarefas(_listaTarefas);
        // console.log(`o item selecionado foi ${idSelecionado}`);
    };


    // o return abaixo, faz parte do lista tarefas, que é renderizado em App.js

    return (
                    
        <>
                    <h1>Lista de Gastos</h1>

            <div id= "container">
                
                

                <div id="lado-esquerdo">

                    <form id="formulario" onSubmit={event =>event.preventDefault()}>
                            <div>
                                <h3>Novo Item</h3>
                                
                                <div className="campo-primario">
                                    <label>Descrição:</label>
                                    <input type="text" onChange={(evento) => setDescricaoNovoItem( evento.target.value)} />                                                                 
                                </div>         

                                <div>                                    
                                    <label>Categoria:</label>
                                    <select 
                                    value={categoriaNovoItem}
                                    onChange={(evento) => setCategoriaNovoItem( evento.target.value) }>                                        
                                        <option>Selecione uma categoria</option>
                                        <OpcoesCategoriasComponente />
                                    </select>                                     
                                </div>

                                <div id="valor-data">
                                    <div className="valor">
                                        <label>Valor</label>
                                        {/* <input type="text" placeholder="10.00" onChange={(evento) => setValorNovoItem( evento.target.value)} /> */}
                                        <CurrencyInput
                                            id="input-example"
                                            name="input-name"
                                            placeholder="10.00"
                                            value={valorNovoItem}
                                            decimalsLimit={2}
                                            prefix= "R$"
                                            decimalSeparator="," groupSeparator="."
                                            onValueChange={(value) => {
                                                setValorNovoItem(value)
                                                // console.log(value)
                                            }}
                                        />
                                    </div>

                                    <div>
                                        <label>Data</label>
                                        <input type="date" onChange={(evento) => setDataNovoItem( evento.target.value)} />
                                    </div>
                                </div>
                            </div>

                            <div id="caixa">                                    
                                    <div>
                                        <p>Turno</p>
                                        <OpcoesTurnoComponente/>                                       
                                    </div>   
                            </div>

                            <div>

                                <p>Alerta?</p>
                                <label><input 
                                        type="checkbox"
                                        id='campo-alerta'
                                        name='campo-alerta'
                                        onChange={ () => {setAlertaNovoItem( alertaNovoItem === 'ligado' ? 'desligado' : 'ligado' ) }} />Ligado</label>
                                
                            </div>                   
                                <button
                                 onClick={ () => incluirItem() }
                                 >Incluir</button>
                    </form>
                </div>

                <div id="lado-direito">


                    <table>
                        <thead>
                            <tr>
                                <th>Descrição</th>
                                <th>Categoria</th>
                                <th>Data</th>
                                <th>Turno</th>
                                <th>Valores</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    
                        <CorpoTabelaComponente/>

                        <tfoot>
                            <tr>
                                {/* <td> Total de Itens :  {listaTarefas.length}/td> */}
                                <td> Total de Itens: {listaTarefas.length}</td>
                                <td colSpan="6"><span>Valor Total é:   {total.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})} </span></td>
                                
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>

        </>
        
    );
}

export default ListaTarefas;

